import re
regex = "[0-9]$"
data = ["word1ahbzb4445", "12zaHeif4sfhb",
        "923Wordb5d4shz", "WordahbbjaiPrAkAshword"]
for each in data:
    result = re.search(regex, each)
    if result:
        print(result.group())
    else:
        print("No Match")
