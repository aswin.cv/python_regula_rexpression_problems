import re
data = 'The quick brown fox jumps over the lazy dog.'
regex = "fox|dog|horse"
result = re.findall(regex, data)
if len(result) > 0:
    print(result)
else:
    print("No Match")
