import re
data = 'The quick brown fox jumps over the lazy dog.'
regex = "fox"
result = re.search(regex, data)
if result:
    print(result.start())
    print(result.end())
    print(result.span())
else:
    print("No Match")
