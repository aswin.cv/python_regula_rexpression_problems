import re
pythoncase = "words_paf_habp_worder_av"
camelcase = pythoncase
regex = "(_)([a-z])"
for each in re.finditer(regex, pythoncase):
    s = each.group(2).upper()
    # print(s)
    camelcase = re.sub(regex, s, camelcase, count=1)
    # print(camelcase)
print(camelcase)
