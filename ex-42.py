import re
data = "Hello www.google.com World http://yahoo.com"
regex = r'(www.)[\w]{1,}.[\w]{3}|(http://)[\w]{1,}.[\w]{3}'
for each in re.finditer(regex, data):
    print(each.group())
