import re
data = ["2.34", "43.3", "875895.745759", "475937.34"]
for each in data:
    regex = r'^\d{1,}.\d{2}$'
    result = re.search(regex, each)
    if result:
        print(result.group())
