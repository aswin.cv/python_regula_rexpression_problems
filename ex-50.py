import re
data = ["example (.com)", "w3resource", "github (.com)",
        "stackoverflow (.com)"]
for each in data:
    replaced_string = re.sub("\(.*\)", "", each)
    print(replaced_string)
